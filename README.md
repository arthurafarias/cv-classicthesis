# README #

This readme will guide you to add this project to your workspace at Eclipse Platform With Egit and TeXlipse properly configured.

If you want to know how to install this there is an excelent guide that will ensure that you will have all environment working properly at ubuntu-linux.

http://askubuntu.com/questions/26632/how-to-install-eclipse

How to install TeXlipse plugin is available at their own website.

http://texlipse.sourceforge.net/manual/installation.html

How to install TeXlipse egit plugin is available at.

http://stackoverflow.com/questions/1623586/installing-git-on-eclipse

### How do I get set up? ###

When you LaTeX distribution and you eclipse environment is installed properly, you should be able to import this git repository just by following those steps.


1. Go to File>Import
2. Type "projects from git" in the search field.
3. Select Git>Projects from Git
4. Click on "Next >" button
5. Choose Clone URI
6. Type your fork this project to your account and put the clone HTTPS url in URI field like https://bitbucket.org/arthur_afarias/cv-classicthesis.git.
7. Put your username or passwork if applicable.
8. Click "Next >"
9. Select a preferable branch, the default branch is the master branch.
10. Choose an appropriate destination directory. Let anything else unchanged.
11. Click "Next >"
12. Choose Import existing Eclipse projects
13. Select cv-classicthesis
14. Click Finish.


You should be able to build the project an see the output pdf in build folder.

### Who do I talk to? ###

* Repo owner: Arthur Farias (arthur@afarias.org)